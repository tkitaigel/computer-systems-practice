long cread(long *xp) {
	return (xp ? *xp : 0);
}

long cread_alt2(long *xp) {
	int r = 0;
	xp && (r = *xp);
	return r;
}

/*
	.file	"190725-3.59-cread.c"
	.text
	.globl	cread
	.type	cread, @function
cread:
.LFB0:
	.cfi_startproc
	movl	$0, %eax
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
.L1:
	rep ret
	.cfi_endproc
.LFE0:
	.size	cread, .-cread
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/

