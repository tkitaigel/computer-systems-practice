#define CNT 7
#define CNTX 4

typedef struct {
	long idx;
	long x[CNTX];
} a_struct;

typedef struct {
	int first;
	a_struct a[CNT];
	int last;
} b_struct;

void test(long i, b_struct *bp)
{
	int n = bp->first + bp->last;
	a_struct *ap = &bp->a[i];
	ap->x[ap->idx] = n;
}
/*
2	mov 0x120(%rsi),%ecx        : ecx = bp->last = bp + 0x120(288)
3	add (%rsi),%ecx		    : n(ecx) = bp->first(rsi) + bp->last(rcx)
4	lea (%rdi,%rdi,4),%rax      : rax = 5i
5	lea (%rsi,%rax,8),%rax      : rax = 8*5i + bp
6	mov 0x8(%rax),%rdx          : rdx = 8 * (40i + bp)
7	movslq %rcx,%rcx	    : nをintからlongに拡張
8	mov %rcx,0x10(%rax,$rdx,8)  : nをap->x[ap->idx]に代入
9	retq
 */

/*
#define CNT 7
#define CNTX 4
	.file	"190807-3.69.c"
	.text
	.globl	test
	.type	test, @function
test:
.LFB0:
	.cfi_startproc
	movl	288(%rsi), %edx
	addl	(%rsi), %edx
	leaq	0(,%rdi,4), %rax
	leaq	(%rax,%rdi), %r8
	leaq	0(,%r8,8), %rcx
	movslq	8(%rsi,%rcx), %rcx
	addq	%rdi, %rax
	addq	%rcx, %rax
	movslq	%edx, %rdx
	movq	%rdx, 16(%rsi,%rax,8)
	ret
	.cfi_endproc
.LFE0:
	.size	test, .-test
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
/* 4, 4 patern
	.file	"190807-3.69.c"
	.text
	.globl	test
	.type	test, @function
test:
.LFB0:
	.cfi_startproc
	movl	168(%rsi), %edx
	addl	(%rsi), %edx
	leaq	0(,%rdi,4), %rax
	leaq	(%rax,%rdi), %r8
	leaq	0(,%r8,8), %rcx
	addq	%rdi, %rax
	addq	8(%rsi,%rcx), %rax
	movslq	%edx, %rdx
	movq	%rdx, 16(%rsi,%rax,8)
	ret
	.cfi_endproc
.LFE0:
	.size	test, .-test
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
