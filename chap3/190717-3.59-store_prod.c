#include <stdio.h>

typedef __int64_t int64_t;
typedef __int128 int128_t;

void store_prod(int128_t *dest, int64_t x, int64_t y) {
	*dest = x * (int128_t) y;
}
/*  ** 違う **
 * X = 2^64 Xh + Xl 、入力64bitsであり、Xh = 0 であるため、X = Xl :式(1)
 * Y = 2^64 Yh + Yl 同じく、Y = Yl :式(2)
 * P = 2^64 Ph + Pl :式(3)
 * Pl は、Pl = Xl * Yl :式(4) と定義できる。
 * store_prod 関数の定義より、P = X * Y であり、式(1), (2)より、
 * P = Xl * Yl :式(5)
 * 式(3) の P と、Pl に、式(5), (4) を代入すると、
 * Xl * Yl = 2^64 Ph + Xl * Yl
 * 2^64 Ph = 0 より、Ph = 0 したがって、P = Pl = Xl * Yl
 */
/*
 * void store_prod (int128_t *dest, int64_t x, int64_t y)
 * func            (rdi,                  rsi,       rdx,...)
 */
/*
	movq	%rdx, %rax      // y(%rdx) の値を %rax にコピー
	cqto			// %rax を、%rdx:%rax の 128bits 長に拡張
	movq	%rsi, %rcx      // Xl を %rcx に代入
	sarq	$63, %rcx	// Xh を作成 (128bits 長を表現する %rcx:%rsi を作成)
	imulq	%rdx, %rcx	// Yh * Xh(Xlらしい？) = %rcx
	imulq	%rsi, %rdx	// Xl * Yh = %rdx
	addq	%rdx, %rcx   	// (Xl * Yh) + (Xh(l?) * Yh) = %rcx
	mulq	%rsi            // Xl * Yl -> %rdx:%rax (128bits拡張)
	addq	%rcx, %rdx      // (Xh * Yh) + (Xl * Yh) + (Xl * Yl)
	movq	%rax, (%rdi)    // x * y の下部64ビット(Pl)をリトルエンディアン形式で保管
	movq	%rdx, 8(%rdi)   // x * y の上位64ビット(Ph)を8バイト移動して保管
	ret
(Xh*2**64 + Xl)(Yh*2**64 + Yl)
XhYh*2**128 + (XlYh + XhYl)*2**64 + XlYl = (A)

  xlyl = j2**64(xlyl>>64) + (xlyl mod64) = t
(A) = 2**64 (xhyl + xlyh + t) + (xlyl mod64)
*/
/*
 * void store_prod (int128_t *dest, int64_t x, int64_t y)
 * func            (rdi,                  rsi,       rdx)
--------------------------------------------------------------------------------
.LFB23:
	.cfi_startproc
	movq	%rdx, %rcx
	sarq	$63, %rcx   // y の符号だけ->rcx
	movq	%rsi, %r8   //
	sarq	$63, %r8    // x の符号だけ->r8
	imulq	%rdx, %r8   // y * xsgn -> r8
	imulq	%rsi, %rcx  // x * ysgn -> rcx
	addq	%r8, %rcx   // (xsgn * y) + (x * ysgn) -> rcx
	movq	%rsi, %rax  // x -> rax
	mulq	%rdx        // y * x -> %rdx:%rax (128bits拡張)
	addq	%rcx, %rdx  // {(xsgn * y) + (x * ysgn)} + (x * y)[h]
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	ret
	.cfi_endproc
.LFE23:
	.size	store_prod, .-store_prod
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
/*
	.file	"190717-3.59-store_prod.c"
	.text
	.p2align 4,,15
	.globl	store_prod
	.type	store_prod, @function
store_prod:
.LFB23:
	.cfi_startproc
	movq	%rsi, %rax
	imulq	%rdx
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	ret
	.cfi_endproc
.LFE23:
	.size	store_prod, .-store_prod
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
