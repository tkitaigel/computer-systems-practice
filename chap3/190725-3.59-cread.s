	.file	"190725-3.59-cread.c"
	.text
	.p2align 5,,31
	.globl	cread
	.type	cread, @function
cread:
.LFB0:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L3
	movq	(%rdi), %rax
	ret
	.p2align 5,,7
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE0:
	.size	cread, .-cread
	.p2align 5,,31
	.globl	cread_alt2
	.type	cread_alt2, @function
cread_alt2:
.LFB1:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L7
	movslq	(%rdi), %rax
	ret
	.p2align 5,,7
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1:
	.size	cread_alt2, .-cread_alt2
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
