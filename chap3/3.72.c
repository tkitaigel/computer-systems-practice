/*
+8	|  Return addr	|
	|---------------|
 0	|  Bak rbp	|  rbp
	|---------------|
-8	|	i	|
	|---------------|
-16	|   (unused)	|
 	|~~~~~~~~~~~~~~~|~~~ s1
-24	|	e1	|
	|---------------|
	|		|
	|	p	|
	|   (8n Byte)	|
	|		|
	|---------------|--- p
	|	e2	|
	|---------------|--- s2

A. Describe how to calculate s2.
B. Describe how to calculate p.
C. Show combinations(n, s1) to be e1 max, or to be min.
D. Describe this code prove what kinds of alignment feature as s2 and p.
*/

A.
s2 = rbp - (8n + 30) & 0xffffff00 - 16
B.
p = 8n

C.
16byte align で常にstackが構成される場合
e1,Max:8 (n, s1) = (1, 16)
e1,Min:0 (n, s1) = (2, 16)
n, p,  s1, s2, e1, e2  (s1は固定？)
--------------------------------
1, 32, 16, 32,  8, 0
2, 32, 16, 32,  0, 0
3, 48, 16, 48,  8, 0
4, 48, 16, 48,  0, 0
--------------------------------
1, 24, 15, 32,  1, 8  (rbp+1)
--------------------------------
1, 40, 17, 32, 15, 8  (rbp+1)

16byte align ではない開始位置からstackが構成されることがある場合
e1,Max:15 (n, s1) = (1, 17)
e1,Min:0 (n, s1) = (2, 16)

D.
アドレスについて以下を保証
  s2 16byte align に沿う
  p   8byte align に沿う
