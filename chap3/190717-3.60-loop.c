#include <stdio.h>

long loop(long x, int n)
{
	long result = 0;
	long mask;
	for (mask = 1; mask != 0; mask = mask << n)
		result |= x & mask;
	return result;
}
/*
A. x	 : %rdi
   n	 : %esi
   result: %rax
   mask	 : %rdx
B. 初期値：result=0, mask=1
C. mask がゼロではないことをテストする。
D. mask は、1から始まり、for ループ毎に n ビット左シフトする。
E. result は、for ループ毎に x を mask でマスク（論理積）した値を足す（論理和）ように更新する。mask はループ毎にnビットシフトされる。
*/

#if 0
	.file	"190717-3.60-loop.c"
	.text
	.globl	loop
	.type	loop, @function
loop:
.LFB23:
	.cfi_startproc
	movl	%esi, %ecx
	movl	$1, %edx
	movl	$0, %eax
	jmp	.L2
.L3:
	movq	%rdx, %r8
	andq	%rdi, %r8
	orq	%r8, %rax
	salq	%cl, %rdx
.L2:
	testq	%rdx, %rdx
	jne	.L3
	rep ret
	.cfi_endproc
.LFE23:
	.size	loop, .-loop
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
#endif
