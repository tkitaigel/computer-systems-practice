	.global find_range

find_range:
        xorq     %rax, %rax
        movl     $0, %eax
        vxorps   %xmm1, %xmm1, %xmm1
        vucomiss %xmm1, %xmm0
	jp 	.OTR
        ja      .POS  # POS case (ret=2)
        je      .ZER  # ZERO case (ret=1)
        jb      .NEG  # NEG case (ret=0)
.OTR:
        setbe   %al   # %al = NaN ? 1 : 0, OTHER case (ret=3)
        movzbl  %al, %eax
.POS:
        addl    $1, %eax
.ZER:
        addl    $1, %eax
.NEG:
        ret
