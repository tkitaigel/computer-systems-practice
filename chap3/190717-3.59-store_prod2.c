#include <stdio.h>

typedef __int64_t int64_t;
typedef __int128 int128_t;

void store_prod(int128_t *dest, int64_t x, int64_t y) {
	*dest = x * (int128_t) y;
}
/*
	.file	"store_prod.c"
	.text
	.globl	store_prod
	.type	store_prod, @function
store_prod:
.LFB23:
	.cfi_startproc
	movq	%rdx, %rcx
	sarq	$63, %rcx
	movq	%rsi, %r8
	sarq	$63, %r8
	imulq	%rdx, %r8
	imulq	%rsi, %rcx
	addq	%r8, %rcx
	movq	%rsi, %rax
	mulq	%rdx
	addq	%rcx, %rdx
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	ret
	.cfi_endproc
.LFE23:
	.size	store_prod, .-store_prod
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
/*
 * void store_prod (int128_t *dest, int64_t x, int64_t y)
 * func            (rdi,                  rsi,     rdx,...)
 */
/*
	movq	%rdx, %rax      // y(%rdx) の値を %rax にコピー
	cqto			// %rax を、%rdx:%rax の128bits 長に拡張
	movq	%rsi, %rcx      // x -> %rcx
	sarq	$63, %rcx	// 右算術シフト sign -> %rcx
	imulq	%rdx, %rcx	// 符号付き掛け算(yh * xsign) -> %rcx
	imulq	%rsi, %rdx	//               (x * yh) -> %rdx
	addq	%rdx, %rcx   	// (x * yh) + (yh * xsign) -> %rcx
	mulq	%rsi            // %rax * %rsi:  (yl * x) -> %rdx:%rax (128bits拡張)
	addq	%rcx, %rdx      // {(x * yh) + (yh * xsign)}+ (yl * x)h -> rdx
                                // (Yh * xsign) + (Xl * Yh) + (Yl + Xh)
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	ret
*/
/*
	movq	%rdx, %rax      // y(%rdx) の値を %rax にコピー
	cqto			// %rax を、%rdx:%rax の128bits 長に拡張
	movq	%rsi, %rcx      // x -> %rcx
	sarq	$63, %rcx	// 右算術シフト(x: rshift:63) xh???
	imulq	%rdx, %rcx	// 符号付き掛け算(yh * xh) -> %rcx
	imulq	%rsi, %rdx	//               (x * yh) -> %rdx
	addq	%rdx, %rcx   	// (x * yh) + (yh * xh) -> %rcx
	mulq	%rsi            // %rax * %rsi:  (yl * x) -> %rdx:%rax (128bits拡張)
	addq	%rcx, %rdx      // {(x * yh) + (yh * xh)}+ (yl * x)h -> rdx
                                // (Yh *Xh) + (Xl * Yh) + (Yl + Xh)
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	ret
*/
/*
Ph=2^64XhYh + (XhYl + XlYh)
Pl=XlYl
---------------------------------
XY = (2^64Xh + Xl)(2^64Yh + Yl)
   = 2^128XhYh + 2^64(XlYh + XhYl)  + XlYl
   = 2^64(2^64XhYh + (XlYh + XhYl)) + XlYl
   = 2^64 Ph                        + Pl
*/
