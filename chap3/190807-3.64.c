#include <stdio.h>

//A.回答 D[R][C][T] = Xd + L(CTi + Tj + k)
//B.回答 R=7, S=5, T=13
long A[7][5][13];

long store_ele (long i, long j, long k, long *dest)
/* i:rdi, j:rsi, k:rdx, dest: rcx */
{
	*dest = A[i][j][k];
	return sizeof(A);
}
/*
	.file	"190807-3.64.c"
	.text
	.globl	store_ele
	.type	store_ele, @function
store_ele:
.LFB23:
	.cfi_startproc
	leaq	(%rsi,%rsi,2), %rax	rsi + 2rsi = 3rsi
	leaq	(%rsi,%rax,4), %rax	rsi + 3rsi * 4rsi = 13 rsi [Tj]
	movq	%rdi, %rsi
	salq	$6, %rsi		64rdi : 2^6=64
	addq	%rsi, %rdi		64rdi + rdi = 65rdi [CTi] <- 5*13=65
	addq	%rax, %rdi		5 * 13 * rdi + 13rsi
	addq	%rdi, %rdx		5 * 13 * rdi [CTi] + 13rsi [Tj] + rdx [k] = D[R][C][T]
	leaq	A(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%rcx)
	movl	$3640, %eax
	ret
	.cfi_endproc
.LFE23:
	.size	store_ele, .-store_ele
	.comm	A,3640,32
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
/*
long A[7][5][13];
	.file	"190807-3.64.c"
	.text
	.globl	store_ele
	.type	store_ele, @function
store_ele:
.LFB23:
	.cfi_startproc
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rsi,%rax,4), %rax
	movq	%rdi, %rsi
	salq	$6, %rsi   2^6=64, 5*13=65を得ようとしている？
                            [5][7][13]だとここが変わり、addq が一つ減る imulq $91, %rdi, %rdi
	addq	%rsi, %rdi
	addq	%rax, %rdi
	addq	%rdi, %rdx
	leaq	A(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%rcx)
	movl	$3640, %eax
	ret
	.cfi_endproc
.LFE23:
	.size	store_ele, .-store_ele
	.comm	A,3640,32
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
/*
long A[8][7][13];
	.file	"190807-3.64.c"
	.text
	.globl	store_ele
	.type	store_ele, @function
store_ele:
.LFB23:
	.cfi_startproc
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rsi,%rax,4), %rax
	imulq	$91, %rdi, %rdi
	addq	%rax, %rdi
	addq	%rdi, %rdx
	leaq	A(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%rcx)
	movl	$5824, %eax
	ret
	.cfi_endproc
.LFE23:
	.size	store_ele, .-store_ele
	.comm	A,5824,32
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
