	.file	"3.73.c"
	.text
	.globl	find_range_on_c
	.type	find_range_on_c, @function
find_range_on_c:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movss	%xmm0, -20(%rbp)
	pxor	%xmm0, %xmm0
	ucomiss	-20(%rbp), %xmm0
	jbe	.L13
	movl	$0, -4(%rbp)
	jmp	.L4
.L13:
	pxor	%xmm0, %xmm0
	ucomiss	-20(%rbp), %xmm0
	jp	.L5
	pxor	%xmm0, %xmm0
	ucomiss	-20(%rbp), %xmm0
	jne	.L5
	movl	$1, -4(%rbp)
	jmp	.L4
.L5:
	movss	-20(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.L14
	movl	$2, -4(%rbp)
	jmp	.L4
.L14:
	movl	$3, -4(%rbp)
.L4:
	movl	-4(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	find_range_on_c, .-find_range_on_c
	.globl	range
	.section	.rodata
.LC1:
	.string	"NEG"
.LC2:
	.string	"ZERO"
.LC3:
	.string	"POS"
.LC4:
	.string	"OTHER"
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	range, @object
	.size	range, 32
range:
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.text
	.globl	find_range2
	.type	find_range2, @function
find_range2:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movss	%xmm0, -20(%rbp)
	pxor	%xmm0, %xmm0
	ucomiss	-20(%rbp), %xmm0
	jbe	.L27
	movl	$0, -4(%rbp)
	jmp	.L18
.L27:
	pxor	%xmm0, %xmm0
	ucomiss	-20(%rbp), %xmm0
	jp	.L19
	pxor	%xmm0, %xmm0
	ucomiss	-20(%rbp), %xmm0
	jne	.L19
	movl	$1, -4(%rbp)
	jmp	.L18
.L19:
	movss	-20(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.L28
	movl	$2, -4(%rbp)
	jmp	.L18
.L28:
	movl	$3, -4(%rbp)
.L18:
	movl	-4(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	find_range2, .-find_range2
	.section	.rodata
	.align 8
.LC5:
	.string	"%u: find_range()->%s, find_range_asm()->%s\n"
.LC6:
	.string	"too many errors. termnating"
.LC7:
	.string	"good=%ld, bad=%ld\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	$0, -32(%rbp)
	movq	$0, -24(%rbp)
.L33:
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -52(%rbp)
	movss	-52(%rbp), %xmm0
	call	find_range2
	movl	%eax, -40(%rbp)
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -52(%rbp)
	movss	-52(%rbp), %xmm0
	call	find_range@PLT
	movl	%eax, -36(%rbp)
	movl	-40(%rbp), %eax
	cmpl	-36(%rbp), %eax
	je	.L30
	movl	-36(%rbp), %eax
	leaq	0(,%rax,8), %rdx
	leaq	range(%rip), %rax
	movq	(%rdx,%rax), %rcx
	movl	-40(%rbp), %eax
	leaq	0(,%rax,8), %rdx
	leaq	range(%rip), %rax
	movq	(%rdx,%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, %esi
	leaq	.LC5(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	addq	$1, -24(%rbp)
	cmpq	$10, -24(%rbp)
	jbe	.L31
	leaq	.LC6(%rip), %rdi
	call	puts@PLT
	jmp	.L32
.L30:
	addq	$1, -32(%rbp)
.L31:
	movl	-44(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -44(%rbp)
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jne	.L33
.L32:
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC7(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, %eax
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	je	.L35
	call	__stack_chk_fail@PLT
.L35:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
