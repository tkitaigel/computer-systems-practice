#include <stdio.h>
#include <stdint.h>

typedef enum {NEG, ZERO, POS, OTHER} range_t;
extern int find_range(float f);

range_t find_range_on_c(float x)
{
	int result;
	if (x < 0)
		result = NEG;
	else if (x == 0)
		result = ZERO;
	else if (x > 0)
		result = POS;
	else
		result = OTHER;
	return result;
}

const char *range[] = {
	[NEG] = "NEG",
	[ZERO] = "ZERO",
	[POS] = "POS",
	[OTHER] = "OTHER",
};

range_t find_range2(float x) {
	int result;

	if (x < 0)
		result = NEG;
	else if (x == 0)
		result = ZERO;
	else if (x > 0)
		result = POS;
	else
		result = OTHER;
	return result;
}

int main() {
	unsigned x = 0;
	float *f = &x;
	uint64_t good = 0;
	uint64_t bad = 0;

	do {
		range_t r1 = find_range2(*f);
		range_t r2 = find_range(*f);
		if (r1 != r2) {
			printf("%u: find_range()->%s, find_range_asm()->%s\n",
				x, range[r1], range[r2]);
			bad++;
			if (bad > 10) {
				printf("too many errors. termnating\n");
				break;
			}
		} else {
			good++;
		}
		x++;
	} while (x != 0);
	printf("good=%ld, bad=%ld\n", good, bad);
}
