#define NR(n)	(1 + 4*(n))
#define NC(n)	(3*(n) > 0 ? 9*(n)<<3 : 0)

long sum_col(long n, long A[NR(n)][NC(n)], long j)
{
	long i;
	long result = 0;
	for (i = 0; i < NR(n); i++)
		result += A[i][j];
	return result;
}
/*
#define NR(n)	(1 + 4*(n))
#define NC(n)	(3*(n) > 0 ? 9*(n)<<3 : 0)
	.file	"190807-3.66.c"
	.text
	.globl	sum_col
	.type	sum_col, @function
sum_col:
.LFB0:
	.cfi_startproc
	leaq	(%rdi,%rdi,2), %rax
	testq	%rax, %rax
	jle	.L5
	leaq	(%rdi,%rdi,8), %r8
	salq	$3, %r8
.L2:
	leaq	1(,%rdi,4), %r9
	movl	$0, %eax
	movl	$0, %ecx
	jmp	.L3
.L5:
	movl	$0, %r8d
	jmp	.L2
.L4:
	movq	%rcx, %rdi
	imulq	%r8, %rdi
	leaq	(%rsi,%rdi,8), %rdi
	addq	(%rdi,%rdx,8), %rax
	addq	$1, %rcx
.L3:
	cmpq	%r9, %rcx
	jl	.L4
	rep ret
	.cfi_endproc
.LFE0:
	.size	sum_col, .-sum_col
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
/*
	.file	"190807-3.66.c"
	.text
	.globl	sum_col
	.type	sum_col, @function
sum_col:
.LFB0:
	.cfi_startproc
	movl	$0, %eax
	movl	$0, %ecx
	jmp	.L2
.L3:
	leaq	(%rcx,%rcx,2), %r8
	leaq	0(,%r8,8), %rdi
	addq	%rsi, %rdi
	addq	(%rdi,%rdx,8), %rax
	addq	$1, %rcx
.L2:
	cmpq	$4, %rcx
	jle	.L3
	rep ret
	.cfi_endproc
.LFE0:
	.size	sum_col, .-sum_col
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
