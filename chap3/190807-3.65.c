#if 0
A. A[i][j]=%rdx
B. A[j][i]=%rax
C. M=15
#endif
#define M 15
void transpose(long A[M][M]) {
	long i, j;
	for (i = 0; i < M; i++)
		for (j = 0; j < i; j++) {
			long t = A[i][i];
			A[i][j] = A[j][i];
			A[j][i] = t;
		}
}
/*
	.file	"190807-3.65.c"
	.text
	.globl	transpose
	.type	transpose, @function
transpose:
.LFB0:
	.cfi_startproc
	leaq	8(%rdi), %r10
	leaq	120(%rdi), %r9
	leaq	128(%rdi), %rcx
	movl	$0, %r8d
	jmp	.L2
.L3:
	movq	(%rcx), %rsi
	movq	(%rax), %rdi
	movq	%rdi, (%rdx)
	movq	%rsi, (%rax)
	addq	$120, %rax
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jne	.L3
.L5:
	addq	$8, %r10
	addq	$120, %r9
	subq	$-128, %rcx
.L2:
	addq	$1, %r8
	cmpq	$15, %r8
	je	.L1
	movq	%r9, %rdx
	movq	%r10, %rax
	testq	%r8, %r8
	jg	.L3
	jmp	.L5
.L1:
	rep ret
	.cfi_endproc
.LFE0:
	.size	transpose, .-transpose
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
*/
