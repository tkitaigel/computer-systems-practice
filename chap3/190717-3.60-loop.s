	.file	"190717-3.60-loop.c"
	.text
	.globl	loop
	.type	loop, @function
loop:
.LFB23:
	.cfi_startproc
	movl	%esi, %ecx
	movl	$1, %edx
	movl	$0, %eax
.L2:
	movq	%rdi, %r8
	andq	%rdx, %r8
	orq	%r8, %rax
	salq	%cl, %rdx
	testq	%rdx, %rdx
	jne	.L2
	rep ret
	.cfi_endproc
.LFE23:
	.size	loop, .-loop
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
