#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <err.h>

#define BUFSIZE 65535
#define DURATION 10

#define N BUFSIZE

static volatile int alrm;

void psum1(float a[], float p[], long n) {
        long i;
        p[0] = a[0];
        for (i = 1; i < n; i++)
                p[i] = p[i-1] + a[i];
}

void psum2(float a[], float p[], long n) {
        long i;
        p[0] = a[0];
        for (i = 1; i + 4 <= n; i += 4) {
                p[i]   = p[i-1] + a[i];
/*
                p[i+1] = p[i] + a[i+1];
                p[i+1] = (p[i-1] + a[i])+ a[i+1];
*/
                p[i+1] = p[i-1] + (a[i] + a[i+1]);
                p[i+2] = p[i-1] + (a[i] + a[i+1] + a[i+2]);
                p[i+3] = p[i-1] + (a[i] + a[i+1] + a[i+2] + a[i+3]);
	}
	for (; i < n; i++)
                p[i] = p[i-1] + a[i];
}

static void
sigalrm (int sig)
{
    alrm = 1;
}

static struct timespec
timeadd (struct timespec t1, struct timespec t2)
{
    t1.tv_sec += t2.tv_sec;
    t1.tv_nsec += t2.tv_nsec;
    if (t1.tv_nsec >= 1000000000l) {
        t1.tv_sec++;
        t1.tv_nsec -= 1000000000l;
    }
    return t1;
}

static struct timespec
timesub (struct timespec t1, struct timespec t2)
{
    t1.tv_sec -= t2.tv_sec;
    if (t1.tv_nsec >= t2.tv_nsec) {
        t1.tv_nsec -= t2.tv_nsec;
    } else {
        t1.tv_sec--;
        t1.tv_nsec = t1.tv_nsec + 1000000000l - t2.tv_nsec;
    }
    return t1;
}

void measurement (void)
{
    float a[N], b[N], c[N];
    for(int i=0;i<BUFSIZE;i++)a[i]=1.;
    for(int i=0;i<BUFSIZE;i++)b[i]=1.;
    for(int i=0;i<BUFSIZE;i++)c[i]=2.;
    struct timespec total1 = { 0, 0 }, total2 = { 0, 0 };
    struct timespec t1, t2, t3;
    unsigned long n = 0;
    int i;
    signal(SIGALRM,sigalrm);
    warnx("start %u seconds test",DURATION);
    alarm(DURATION);
    while(!alrm){
        clock_gettime (CLOCK_MONOTONIC, &t1);
        psum1 (a, c, BUFSIZE - 1);
        clock_gettime (CLOCK_MONOTONIC, &t2);
        psum2 (b, c, BUFSIZE - 1);
        clock_gettime (CLOCK_MONOTONIC, &t3);
        total1 = timeadd (total1, timesub (t2, t1));
        total2 = timeadd (total2, timesub (t3, t2));
        n++;
    }
    errx (0, "n: %lu \n psum1 : %ld.%09ld\n psum2: %ld.%09ld",
          n, total1.tv_sec, total1.tv_nsec, total2.tv_sec, total2.tv_nsec);
}

int check (float master[], float p[], long n) {
        for (int i = 0; i < n; i++) {
                if (p[i] != master[i]) {
                        printf("test failed at %d. (%f should be %f)\n", i, p[i], master[i]);
                        return -1;
                }
        }
        return 0;
}

int main (void) {
        float a[N], p[N], master[N];

        for (int i = 0; i < N; i++) {
                p[i] = (float)i;
                a[i] = (float)i;
                master[i] = (float)i;
	}

        psum1(master, p, N);
        psum2(a, p, N);
        if (check(master, a, N) < 0)
                return 1;

        printf("passed.\n");
	measurement();
        return 0;
}

