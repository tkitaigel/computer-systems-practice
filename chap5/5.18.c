#include <stdio.h>

double poly(double a[], double x, long degree) {
        long i;
        double result = a[0];
        double xpwr = x; /* Equals x^i at start of loop */
        for (i = 1; i <= degree; i++) {
                result += a[i] * xpwr;
                xpwr = x * xpwr;
        }
        return result;
}

double polyh(double a[], double x, long degree) {
        long i;
        double result = a[degree];
        for (i = degree - 1; i >= 0; i--)
                result = a[i] + x * result;
        return result;
}

double poly2_2x1(double a[], double x, long degree) {
        long i;
        double result = 0;
        double xpwr = 1; /* Equals x^i at start of loop */
        for (i = 0; i + 1 <= degree; i += 2) { /* 2x1 アンローリング */
                result += (a[i] * xpwr + a[i + 1] * xpwr * x);
                xpwr = x * x * xpwr;
	}
        if (i < degree) /* 最後の余った1要素がある場合、それを計算 */
                result += a[i] * xpwr;
        return result;
}

double poly2(double a[], double x, long degree) {
        long i;
        double result  = 0;
        double result1 = 0;
        double xpwr = 1; /* Equals x^i at start of loop */

        /* 2x2 アンローリングで a[n], a[n+1], a[n+2], a[n+3]をループ毎に計算 */
        for (i = 0; i + 3 <= degree; i += 4) {
                result  += (a[i] * xpwr + a[i + 1] * x * xpwr);
                result1 += (a[i + 2] * x * x * xpwr +
			    a[i + 3] * x * x * x * xpwr);
                xpwr = x * x * x * x * xpwr;
	}
	result += result1;

 	/* 2x2 アンローリングから溢れた 1〜3 個の余りを計算 */
        for (; i <= degree; i++)
                result += a[i] * xpwr;
       return result;
}

double polyh2(double a[], double x, long degree) {
        long i;
        double result = 0;
        for (i = degree; i > 0; i -= 2) /* 2x1a アンローリング */
                result = a[i - 1] + x * a[i] + x * x * result;
		/*
		 * R2 = a1 + x * R1 を R3 = a2 + x * R2 に代入した結果の
		 * R3 = a2 + x * (a1 + x * R1) を展開し、
		 * R3 = a2 + x * a1 + x * x * R1 とすることで、
		 * 一つ前のイテレーションに依存しない演算が増える。
		 * その結果、パイプラインをより効率的に演算処理で埋めること
		 * ができ、性能がレイテンシ限界からスループット限界へと近づく。
		 */
        if (i >= 0) /* 最後1つの要素が余る場合、その1つを計算 */
                result = a[i] + x * result;
        return result;
}

void test(double a[], double x, long degree) {
        printf("x=%lf, degree=%ld\n", x, degree);

        printf("poly() = %lf\n", poly(a, x, degree));
        printf("poly2() = %lf\n", poly2(a, x, degree));
        printf("polyh() = %lf\n", polyh(a, x, degree));
        printf("polyh2() = %lf\n", polyh2(a, x, degree));
}

#define DEGREE 2

int main() {
        double a[DEGREE];

        for (int i = 0; i < DEGREE; i++)
                a[i] = (double)i+1;

        test(a, 1.0, DEGREE);
        putchar('\n');
        test(a, 1.0, DEGREE-1);
}
