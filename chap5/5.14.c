#include <stdio.h>

typedef long data_t;
typedef struct {
	long len;
	data_t *data;
} vec_rec, *vec_ptr;

long vec_length(vec_ptr vp)
{
	return vp ? vp->len : -1;
}

data_t *get_vec_start(vec_ptr a)
{
	return a ? a->data : NULL;
}

/* 5.14 */
void inner4_6x1(vec_ptr u, vec_ptr v, data_t *dst)
{
	long i;
	long length = vec_length(u);
	data_t *udata = get_vec_start(u);
	data_t *vdata = get_vec_start(v);
	data_t sum = (data_t) 0;

	for (i = 0; i + 6 <= length; i += 6) {
		sum += udata[i] * vdata[i];
		sum += udata[i+1] * vdata[i+1];
		sum += udata[i+2] * vdata[i+2];
		sum += udata[i+3] * vdata[i+3];
		sum += udata[i+4] * vdata[i+4];
		sum += udata[i+5] * vdata[i+5];
	}
	for (; i < length; i++)
		sum += udata[i] * vdata[i];
	*dst = sum;
}
/*
 * A.
 * どのスカラバージョンの内積計算であっても、InterCore i7 Haswell プロセッサで
 * は、CPEが1.00を下回らない理由を説明せよ。
 * Ans:
 *  変数 sum への可算が並行に処理できないため、CPEは整数の可算コスト1が上限
 *  となる。
 *
 * B.
 * 浮動小数点データに対してループ・アンローリングによる性能向上がない理由を説明せよ
 * Ans:
 *   乗算は、6x1のアンローリングにより並列化されているが、sum への可算は、
 *   一つ前の行の演算が終了した結果を必要とし、並行に実行できない。
 *   したがって、浮動小数点の可算レイテンシ3が、CPEとなる。
 */

/* 5.15 */
void inner4_6x6(vec_ptr u, vec_ptr v, data_t *dst)
{
	long i;
	long length = vec_length(u);
	data_t *udata = get_vec_start(u);
	data_t *vdata = get_vec_start(v);
	data_t sum0, sum1, sum2, sum3, sum4, sum5;
	sum0 = sum1 = sum2 = sum3 = sum4 = sum5 = (data_t) 0;

	for (i = 0; i + 6 <= length; i += 6) {
		sum0 += udata[i] * vdata[i];
		sum1 += udata[i+1] * vdata[i+1];
		sum2 += udata[i+2] * vdata[i+2];
		sum3 += udata[i+3] * vdata[i+3];
		sum4 += udata[i+4] * vdata[i+4];
		sum5 += udata[i+5] * vdata[i+5];
	}
	for (; i < length; i++)
		sum0 += udata[i] * vdata[i];
	*dst = sum0 * sum1 * sum2 * sum3 * sum4 * sum5;
}
/*
 * 誤
 * 可算する先をsum0-5に分散させたことで、(浮動小数点の)可算が並行化され、CPE=1になる。
 * 可算する先をsum0-5に分散させたことで、(浮動小数点の)乗算が並行化され、CPE=1になる。
 *
 * 正
 * 浮動小数点加算のスループット限界のCPEの1を越えられない。
 */

/* 5.16 */
void inner4_6x1a(vec_ptr u, vec_ptr v, data_t *dst)
{
	long i;
	long length = vec_length(u);
	data_t *udata = get_vec_start(u);
	data_t *vdata = get_vec_start(v);
	data_t sum = (data_t) 0;

	for (i = 0; i + 6 <= length; i += 6)
		sum += (((((((udata[i] * vdata[i])   + (udata[i+1] * vdata[i+1]))
		        + (udata[i+2] * vdata[i+2])) + (udata[i+3] * vdata[i+3]))
			+ (udata[i+4] * vdata[i+4])) + (udata[i+5] * vdata[i+5])));
	for (; i < length; i++)
		sum += udata[i] * vdata[i];
	*dst = sum;
}

void main()
{
	data_t a[]={1, 2};
	data_t b[]={4, 2};
	data_t ret;

	vec_rec v, u;
	v.len = 2;
	v.data = (data_t*)&a;
	u.len = 2;
	u.data = (data_t*)&b;
	inner4_6x1(&u, &v, &ret);
	printf ("ans: %ld\n", (long)ret);
}
