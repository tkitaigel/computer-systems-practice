#include <stdio.h>

typedef long data_t;
typedef struct {
	long len;
	data_t *data;
} vec_rec, *vec_ptr;

long vec_length(vec_ptr vp)
{
	return vp ? vp->len : -1;
}

data_t *get_vec_start(vec_ptr a)
{
	a->data;
}

void inner4(vec_ptr u, vec_ptr v, data_t *dst)
{
	long i;
	long length = vec_length(u);
	data_t *udata = get_vec_start(u);
	data_t *vdata = get_vec_start(v);
	data_t sum = (data_t) 0;

	for (i = 0; i < length; i++)
		sum = sum + udata[i] * vdata[i];
	*dst = sum;
}

void main()
{
}
