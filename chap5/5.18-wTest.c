#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <err.h>

static volatile int alrm;

double poly(double a[], double x, long degree) {
        long i;
        double result = a[0];
        double xpwr = x; /* Equals x^i at start of loop */
        for (i = 1; i <= degree; i++) {
                result += a[i] * xpwr;
                xpwr = x * xpwr;
        }
        return result;
}

double polyb(double a[], double x, long degree) {
        long i;
        double result, result1, result2, result3;
        result = result1 = result2 = result3 = 0;
	double x2 = x * x;
	double x3 = x * x * x;
	double x4 = x * x * x * x;
	double x5 = x * x * x * x * x;
        double xpwr = 1; /* Equals x^i at start of loop */
#if 1
        for (i = 0; i + 3 <= degree; i += 4) { /* 2x2 アンローリング */
                result += (a[i] * xpwr + a[i + 1] * x * xpwr);
                result1 += (a[i + 2] * x2 * xpwr + a[i + 3] * x3 * xpwr);
                xpwr *= x4;
        }
        result += result1;
#else
        for (i = 0; i + 1 <= degree; i += 2) {
                result += (a[i] * xpwr + a[i + 1] * x * xpwr);
                xpwr *= x2;
	}
#endif
        for (; i <= degree; i++)
                result += a[i] * xpwr;
        return result;
}

double polyh(double a[], double x, long degree) {
        long i;
        double result = a[degree];
        for (i = degree - 1; i >= 0; i--)
                result = a[i] + x * result;
        return result;
}

double polyh2(double a[], double x, long degree) {
        long i;
        double result = 0;
	double x2 = x * x;
        for (i = degree; i > 0; i -= 2) /* 2x1a アンローリング */
                result = a[i - 1] + x * a[i] + x2 * result;
                /*
                 * R2 = a1 + x * R1 を R3 = a2 + x * R2 に代入した結果の
                 * R3 = a2 + x * (a1 + x * R1) を展開し、
                 * R3 = a2 + x * a1 + x * x * R1 とすることで、
                 * 一つ前のイテレーションに依存しない演算が増える。
                 * その結果、パイプラインをより効率的に演算処理で埋めること
                 * ができ、性能がレイテンシ限界からスループット限界へと近づく。
                 */
        if (i >= 0) /* 最後1つの要素が余る場合、その1つを計算 */
                result = a[i] + x * result;
        return result;
}

static void
sigalrm (int sig)
{
    alrm = 1;
}

static struct timespec
timeadd (struct timespec t1, struct timespec t2)
{
    t1.tv_sec += t2.tv_sec;
    t1.tv_nsec += t2.tv_nsec;
    if (t1.tv_nsec >= 1000000000l) {
        t1.tv_sec++;
        t1.tv_nsec -= 1000000000l;
    }
    return t1;
}

static struct timespec
timesub (struct timespec t1, struct timespec t2)
{
    t1.tv_sec -= t2.tv_sec;
    if (t1.tv_nsec >= t2.tv_nsec) {
        t1.tv_nsec -= t2.tv_nsec;
    } else {
        t1.tv_sec--;
        t1.tv_nsec = t1.tv_nsec + 1000000000l - t2.tv_nsec;
    }
    return t1;
}

#define BUFSIZE 65536
#define DURATION 10

int
main (int argc, char *argv[])
{
    double a[16];
    for(int i=0;i<16;i++)a[i]=1.;
    warnx("65535 test: %f %f",poly(a,2.,15),polyb(a,2.,15));
    warnx("8191 test: %f %f",poly(a,2.,12),polyb(a,2.,12));
    warnx("1111111111111111 test: %f %f",poly(a,10.,15),polyb(a,10.,15));
    warnx("11111 test: %f %f",poly(a,10.,4),polyb(a,10.,4));
    for(int i=0;i<=9;i++)a[i]=i;
    warnx("9876543210 test: %f %f",poly(a,10.,9),polyb(a,10.,9));

    static double buf[BUFSIZE];
    struct timespec total1 = { 0, 0 }, total2 = { 0, 0 },
		total3 = { 0, 0 }, total4 = { 0, 0 };
    struct timespec t1, t2, t3, t4, t5;
    unsigned long n = 0;
    double r1 = 0, r2, r3, r4;
    int i;
    for(i=0;i<BUFSIZE;i++)
        buf[i]=1./16*i;
    signal(SIGALRM,sigalrm);
    warnx("start %u seconds test",DURATION);
    alarm(DURATION);
    while(!alrm){
        clock_gettime (CLOCK_MONOTONIC, &t1);
        r1 = poly (buf, -1., BUFSIZE - 1);
        clock_gettime (CLOCK_MONOTONIC, &t2);
        r2 = polyh (buf, -1., BUFSIZE - 1);
        clock_gettime (CLOCK_MONOTONIC, &t3);
        r3 = polyb (buf, -1., BUFSIZE - 1);
        clock_gettime (CLOCK_MONOTONIC, &t4);
        r4 = polyh2 (buf, -1., BUFSIZE - 1);
        clock_gettime (CLOCK_MONOTONIC, &t5);
        total1 = timeadd (total1, timesub (t2, t1));
        total2 = timeadd (total2, timesub (t3, t2));
        total3 = timeadd (total3, timesub (t4, t3));
        total4 = timeadd (total4, timesub (t5, t4));
        if (r1 != r2 || r2 != r3 || r3 != r4)
            errx (1, "r1 %f r2 %f r3 %f R4 %f", r1, r2, r3, r4);
        n++;
    }
    errx (0, "n: %lu  result: %f\n"
          "poly : %ld.%09ld\npolyb: %ld.%09ld\n"
          "polyh: %ld.%09ld\npolyh2: %ld.%09ld", n, r1,
          total1.tv_sec, total1.tv_nsec, total3.tv_sec, total3.tv_nsec,
          total2.tv_sec, total2.tv_nsec, total4.tv_sec, total4.tv_nsec);
}
