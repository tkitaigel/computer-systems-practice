#include <stdio.h>
//#define MEMSET basic_memset
#define MEMSET memset_x


void *basic_memset(void *s, int c, size_t n)
{
	size_t cnt = 0;
	unsigned char *schar = s;
	while (cnt < n) {
		*schar++ = (unsigned char) c;
		cnt++;
	}
	return s;
}
/*

*/
void *memset_x(void *s, int c, size_t n)
{
	unsigned long *schar = s;
	int k = sizeof(unsigned long);
	unsigned long k_pack = 0;
	int shift;
	for (shift = 0; shift < k; shift++)
		k_pack |= (0xff & (unsigned long)c) << (shift * 8);

	/* align 8 byte alignment */
	size_t cnt;
	unsigned int a = 0x7 & (k - ((unsigned long)s & 0x7));
	char *p = (char *)schar;

	for (cnt = 0; cnt < n && a > 0; cnt++, a--) {
		*p++ = (char)c;
	}
	schar = (unsigned long *)p;

	/* set packed data */
	unsigned long d = 2 * k;
	for (; cnt + d <= n; cnt += d) {
		*schar++ = k_pack;
		*schar++ = k_pack;
	}
	for (; cnt + k <= n; cnt += k) {
		*schar++ = k_pack;
	}

	/* last a few bytes */
	p = (char *)schar;
	for (; cnt < n; cnt++) {
		*p++ = (char)c;
	}
	return s;
}

void test(int offset, int c, size_t n) {
	int i;
	char buffer[2048] = { 0 };

	MEMSET(buffer + offset, c, n);
	printf("memset_x(%p, %d, %.2ld) = ", buffer + offset, c, n);
	for (i = 0; i < sizeof(buffer); i++)
		printf("%02x ", buffer[i]);
	putchar('\n');
}

int main() {
	test(0, 0, 0);
	test(0, 0, 1);
	test(0, 0, 7);
	test(0, 0, 8);
#  if 0
	test(0, 0, 16);
	test(0, 0, 17);
	test(0, 0, 23);
	test(1, 0, 1);
	test(1, 0, 7);
	test(2, 0, 7);
	test(3, 0, 16);
	test(4, 0, 16);
	test(5, 0, 16);
	test(6, 0, 16);
	test(7, 0, 16);
	test(8, 0, 18);
	test(8, 0xf, 18);
	test(8, 0xf, 30);
	test(8, 0x7f, 60);
	test(8, 0x7f, 199);
	test(8, 0x7f, 200);
	test(8, 0x7f, 201);
#  endif
}
