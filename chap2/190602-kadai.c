#include <stdio.h>
#include <limits.h>
int Q261A_anybit_is_1(int arg) {
        int shift = (sizeof(int)<<3) - 1;
        return  (arg & INT_MIN >>shift);
}

/* Q2.62 */
int int_shifts_are_arithmetic(void) {/* arithmetic shift env:1, others:0 */
        return -1 >> 1 == -1;
}

/* Q2.63 */
unsigned srl(unsigned x, int k) {/* shift logically */
        /* Perform shift arithmetically */
        unsigned xsra = (int) x >> k;
        int w = 8*sizeof(int);
        /*
         * -1 means 0xffff(w=16) in two's compliment.
         * Shift left -1(0xffff) until mask width I want.
         * Reverse all bits in the shifted value to get mask, then mask AND xsra.
         */
        return xsra & ~(-(k != 0) << (w - k));
}
int sra(int x, int k) { /* shift arithmetic */
        /* Perform shift logically */
        int sxrl = (unsigned) x >> k;
        int w = 8*sizeof(int);
        int sgnbit = ((x & 1 << (w - 1)) == (1 << (w - 1)));
        return sxrl | (-sgnbit << (w - k));
}

/* Q2.65 */
int ret_1_odd_num_of_1(unsigned x) {/* Return 1 when any odd bit of x equals 1;
                                     * 0 otherwise.(Assume w=32) */
        /*
         * 8bits 分の各数値(0-255)に対応するビット数のテーブル
         * Ex. ctbl[1]=1, ctbl[7]=3, ctbl[255]=8
         */
        int ctbl[] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,
                      1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                      1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                      2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                      1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                      2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                      2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                      3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                      1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
                      2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                      2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                      3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                      2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
                      3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                      3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
                      4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8};
        int bcnt = 0;
        bcnt =    ctbl [ x       & 0xff ] + ctbl [ x >> 8  & 0xff ]
                + ctbl [ x >> 16 & 0xff ] + ctbl [ x >> 24 & 0xff ];
        return bcnt & 0x1;
}

/* Q2.69 */
int rotate_left(unsigned x, int n) {
        unsigned mask = (int)0x80000000 >> (n - 1 + (n == 0));
        return (x << n | (mask & x) >> (32 - n - ((n == 0) << 5)));
}

void main(void) {
#ifdef TEST_Q265
	printf ("ans=%d\n", ret_1_odd_num_of_1(0x0f030303));
	printf ("ans=%d\n", ret_1_odd_num_of_1(0x0703030f));
	printf ("ans=%d\n", ret_1_odd_num_of_1(0xff03030f));
	printf ("ans=%d\n", ret_1_odd_num_of_1(0x0783130f));
#endif
#ifdef TEST_Q263
        printf ("srl(m8 ):%08x\n", srl(0x80000000, 8));
        printf ("srl(m16):%08x\n", srl(0x80000000, 16));
        printf ("srl(m16):%08x\n", srl(0x80000000, 31));
        printf ("srl(m0 ):%08x\n", srl(0x80000000, 0));
        printf ("srl(8  ):%08x\n", srl(0x12300000, 8));
        printf ("srl(16 ):%08x\n", srl(0x12300000, 16));
        printf ("srl(16 ):%08x\n", srl(0x12300000, 31));
        printf ("srl(0  ):%08x\n", srl(0x12300000, 0));
        printf ("sra:%08x\n", sra(0x80000000, 8));
        printf ("sra:%08x\n", sra(0x80000000, 31));
        printf ("sra:%08x\n", sra(0x80000000, 16));
        printf ("sra:%08x\n", sra(0x80000000, 0));
        printf ("sra:%08x\n", sra(0x12300000, 8));
        printf ("sra:%08x\n", sra(0x12300000, 16));
        printf ("sra:%08x\n", sra(0x12300000, 0));
#endif

#ifdef TEST_Q269
        printf ("%x\n", rotate_left(0x12345678, 0));
        printf ("%x\n", rotate_left(0x12345678, 4));
        printf ("%x\n", rotate_left(0x12345678, 20));
        printf ("%x\n", rotate_left(0x12345678, 24));
        printf ("%x\n", rotate_left(0x12345678, 31));
#endif
}
