#include <stdio.h>
#include <limits.h>

/* 2.70 */
int fits_bits(int x, int n)
{
  /* Return 1 when x can be represented as an n-bit, 2's compliment number;
   * 0 otherwise. Assume 1  <= n <=w
   */
   /* x の符号ビットから作成したフィルタで XOR して x<0 の場合の
    * 上位ビット(nの符号ビットを含む範囲)を反転し、
    * n ビットの２の補数表現において、符号ビットを除く範囲である
    * 0 〜 (n - 1) ビットの範囲よりはみ出したビット(1)を検出できる
    * ようにする。 */
   x ^= (INT_MIN & x) >> ((sizeof(int) << 3) - n);
   /* 0 〜 (n - 1) の外にはみ出したビットを検出するマスクを作成 */
   unsigned mask = -1 >> (n - 1) << (n - 1);
   return !(mask & x);
}
#if 0
w=5, n=3
  x         x ^ (singbit >> (w - n))
11111  -1   00011 |
11110  -2   00010 |
11101  -3   00001 |--> OK
11100  -4   00000 |
11011  -5   00100 NG
00000   0   |
00001   1   |
00010   2   |--> OK
00011   3   |
00100   4   NG
#endif

/* 2.73 */
#if 0
int saturating_add(int x, int y)
{
  /* Addition that saturates to TMin or TMax */
  // x + y - Tmax > 0 : Overflow upper
  // x - y + Tmax < Tmax
  int w = sizeof(int)<<3;
  int sgnb = 1 << (w - 1);
  // unsigned over = ~((x + y) & sgnb);
  // return (x + y);
  int f = !((x & w) ^ (y & w));
  //((x & w) ^ (y & w)) && printf ("r=%d\n", (x + y)) || printf ("overflow\n");
  int r;
  //r = ((x & w) ^ (y & w)) && r = 100;
  return r;
}
int sadd(int x, int y)
{
  int sbit = (sizeof(int) << 3) - 1;
  int sum = x + y;
  /* overflow したら ov は -1 */
  int ov = (((x ^ sum) & (y ^ sum))) >> sbit;
  return (ov << sbit) ^ (sum >> ov);
}
#endif

/* Q2.73 */
int saturating_add(int x, int y)
{
    /*
     * x と y の正負を判定するため符号ビットを取得
     * x + y の結果の符号ビットをOverFlow識別のため取得
     */
    int sgn_x = INT_MIN & x;
    int sgn_y = INT_MIN & y;
    int sgn_s = INT_MIN & (x + y);
    /* OverFlow していなければ、 0xff..ff
     * した場合は 0xff..ff + 1 で 0 というマスク */
    int pmask = (~!(!sgn_x && !sgn_y && sgn_s) + 1);
    int nmask = (~!(sgn_x && sgn_y && !sgn_s) + 1);

    return nmask & pmask & (x + y) /* No overflow: (x + y) を返す */
            | (~pmask & INT_MAX)   /* 正の overflow */
            | (~nmask & INT_MIN);  /* 負の overflow */
}

#if 0
int saturating_add(int x, int y)
{
    int w = sizeof(int) << 3;
    int msb = 1 << (w-1);

    int sum = x + y;
    int sign_x = msb & x;
    int sign_y = msb & y;
    int sign_s = msb & sum;

    int nflow = sign_x && sign_y && !sign_s;
    int pflow = !sign_x && !sign_y && sign_s;

    int nmask = (~!nflow + 1);
    int pmask = (~!pflow + 1);
    printf ("mask (n,p)=(%x,%x)", nmask, pmask);

    return (nmask & ((pmask & sum) | (~pmask & ~msb))) | (~nmask & msb);
}
x=011
y=011  sum=110 over

// Overflow case
(011 ^ 110) & (011 ^ 110)
101 & 101 => 101 >shift> 111
// OK
(010 ^ 011) & (001 ^ 011)
001 &  010 => 011 >shift> 000

#endif
#define DFITS_BITS
//#define DSADD
void main()
{
  int i;
#if defined(DFITS_BITS)
  for (i=-5; i < 5;i++)
    printf("[%02d, 3]ret=%d\n", i, fits_bits(i, 3));
  for (i=-5; i < 5;i++)
    printf("[%02d, 2]ret=%d\n", i, fits_bits(i, 2));
  printf ("[ 0, 1]ret=%d\n", fits_bits(0, 1));
  printf ("[-1, 1]ret=%d\n", fits_bits(-1, 1));
  printf ("[ 1, 1]ret=%d\n", fits_bits(1, 1));
#endif
#if defined DSADD
  printf ("%08x\n", saturating_add(0x7ffffff0, 0xff));
  printf ("%08x\n", saturating_add(0x7ffffff0, 0x1));
  printf ("%08x\n", saturating_add(0xfffffff0, 0xffffffff));
  printf ("%08x\n", saturating_add(0xfffffff0, 0xfffffff0));
  printf ("%08x\n", saturating_add(0xffff0000, 0xffff0000));
  printf ("%08x\n", saturating_add(0x80000000, 0x800f0000));
  printf ("%08x\n", saturating_add(0x80000000, 0xffffffff));
#endif
}
