#include <stdio.h>
#include <limits.h>

int divide_power2(int x, int k)
{
  /* !! で、符号ビットの状態を 0,1 の2値へと変換
   * 1つ目の ! で、負数の場合、 (x & INT_MIN) は、 符号ビットが確実に
   * 残るので、! により、0になり、
   * 正数の場合、 (x & INT_MIN) は、全ビット0になるため、! により、1になる。
   * 負数の場合に1となるよう 0,1 を反転させるため、２つ目の ! を使用し、
   * 負： sgn=1、正: sgn=0 を得る。
   */
  int sgn = !!(x & INT_MIN);
  /* (sgn << k) - sgn で、負数の場合のバイアスを算出(2のk乗 - 1)
   * 正数の場合は、sgn=0なので、 バイアス部は、(0 << k - 0) = 0
   */
  return (x + ((sgn << k) - sgn)) >> k;
}

int mul3div4(int x)
{
  int x3 = (x << 2) - x; /* x=INT_MAX/2 等でオーバーフロー */
  int sgn = !!(x3 & INT_MIN);
  return x3 + ((sgn << 2) - sgn) >> 2;
}

int threefourths(int x)
{
  /* 負数の場合は、sgn=0、正数の場合は、sgn=1 */
  int sgn = !!(x & INT_MIN);
  /*
   * 正数の場合の誤差を調整 (d = 前半部 + 後半部 = 0 or 1)
   *   前半部：(1 - sgn) : 正の場合に、1-0 = 1
   *   後半部：(!(x & 3) & !sgn) : 4 で割り切れる場合の調整
   *   従って、4 で割り切れる場合、後半部 1 になり、d = (1-0)-1 = 0
   *        4 で割り切れない場合、後半部 1 になり、d = (1-0)-0 = 1
   * 負数の場合は、d = (1-1)-0 = 0となる。
   */
  int d = (1 - sgn) - (!(x & 3) & !sgn);
  /* 3/4x = x - 1/4x - (誤差) */
  return x - (x >> 2) - d;
}

void main(void)
{
    int i, j;
#if defined DBG_DIVP2
    for (i = -4; i < 10; i++)
      for (j = 0; j < 5; j++)
        printf ("divpw2[%d,%d]=%d\n", i, j, divide_power2(i, j));
#endif
    for (i = 0; i < 10; i++)
      printf ("3/4*(%d)=%d\n", i, mul3div4(i));

    printf ("3/4*(%d)=%d\n", i, mul3div4(4));
    i = INT_MAX/2;
    printf ("3/4*(%d)=%d\n", i, mul3div4(i));
    for (i = -7; i <= 7;i++) {
      int v = mul3div4(i);
      printf ("3/4*(%d)=%d ==>", i, v);
      int w = threefourths(i);
      printf ("threefourths: 3/4*(%d)=%d: %s\n", i, w, v!=w?"ERROR":"");
    }
}
