#include <stdio.h>
#include <stdint.h>
#include <math.h>
typedef unsigned float_bits;

#define FLOAT_FUNC(X) float_twice_f((X))
#define TEST_FUNC(X)  float_twice((X))

/* Q2.92 */
float_bits float_negate(float_bits f)
{
	return (f & 0x7f800000) && (f & 0x7fffff != 0) ? f :
		f ^ (1 << (sizeof(int) << 3) - 1);
}

/* Q2.93 */
float_bits float_absval(float_bits f)
{
	return f & ((unsigned)-1 >> 1);
}

/* Q2.94 */
float_bits float_twice(float_bits f)
{
	float_bits exp_part = 0x7f800000;
	float_bits frac_part = 0x7fffff;
	float_bits ret;
	if ((f & exp_part) == exp_part && (f & 0x7fffff)) {
		ret = f; /* NaN */
	} else if ((f & exp_part) == 0x7f000000) {
		ret = exp_part; /* 無限 */
	} else if (f & exp_part) {
 		/* 正規化数: 指数部のシフトで2倍を反映 */
		ret = (f & exp_part) + (1 << 23) | (f & 0x007fffff);
	} else {
		/* 非正規化数:指数部0なので、仮数部を2倍 */
		ret = (f & 0x007fffff) << 1;
	}
	return (f & (1 << (sizeof(int) << 3) - 1)/* 符号 */) | ret;
}

/* Q2.95 */
float_bits float_half(float_bits f)
{
	float_bits s = f & (1 << (sizeof(int) << 3) - 1);
	return s | (							/* 符号 */
			((f & 0x7f800000) == 0x7f800000) ? f :		/* nan  */
			((f & 0x7f800000) == 0x7f000000) ? 0x7f800000 :	/* 無限 */
			(f & 0x7f800000) ? (f & 0x7f800000) - (1 << 23) | (f & 0x007fffff) : /* 正規化数 */
			((f & 0x007fffff) >> 1) + 0/* 非正規化数 */
		);
}

/* Q2.96 */
int float_f2i(float_bits f)
{
	float_bits exp = (f & 0x7f800000) >> 23;

	return ((f & 0x7f800000) == 0x7f800000) ? f :		/* nan  */
		((f & 0x7f800000) == 0x7f000000) ? 0x7f800000 :	/* 無限 */
		exp > 126 ? (1 << exp - 127) + ((f & 0x00700000 && exp > 127) ? 1 : 0) : 0;
}

int float_f2i_f(float f)
{
	return (int)f;
}

float float_twice_f(float f)
{
       return (isnan(f)) ? f : f * 2.0;
}

float float_half_f(float f)
{
       return (isnan(f)) ? f : f / 2.0;
}

#if 1
void test(void)
{
       uint64_t bad = 0;
       uint64_t good = 0;

       unsigned x = 0;
       do {
               float *fp = (float *)&x;
               float f = FLOAT_FUNC(*fp);
               float_bits fb = TEST_FUNC(x);

               unsigned *ff = (unsigned *)&f;
               if (fb != *ff) {
                       bad++;
                       printf("  %f (%.8x): %.8x != %.8x (good=%lu)\n", *fp, x, fb, *ff, good);
               } else {
                       good++;
                       //printf("G:%f (%.8x): %.8x != %.8x (good=%lu)\n", *fp, x, fb, *ff, good);
               }
       } while(++x);

       printf("good=%lu, bad=%lu\n", good, bad);
}
#else
void test(void)
{
       uint64_t bad = 0;
       uint64_t good = 0;

       unsigned x = 0;
       do {
               float *fp = (float *)&x;
               int i = FLOAT_FUNC(*fp);
               float_bits fb = TEST_FUNC(x);
               if (fb != i) {
                       bad++;
                       printf("  %f (%.8x): %.8x != %.8x (good=%lu)\n", *fp, x, fb, i, good);
               } else {
                       good++;
               }
       } while(++x);

       printf("good=%lu, bad=%lu\n", good, bad);
}

#endif
void main(void)
{
	/*
	printf ("your:%x\n", TEST_FUNC(0x00800004));
	float fp = (float)0x00800004;
	printf ("_f  :%x\n", (unsigned)FLOAT_FUNC(fp));
	*/
	printf ("-------------------------\n");
	test();
}
