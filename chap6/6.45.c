#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <err.h>
#include <stdlib.h>

static volatile int alrm;

static void
sigalrm (int sig)
{
    alrm = 1;
}

static struct timespec
timeadd (struct timespec t1, struct timespec t2)
{
    t1.tv_sec += t2.tv_sec;
    t1.tv_nsec += t2.tv_nsec;
    if (t1.tv_nsec >= 1000000000l) {
        t1.tv_sec++;
        t1.tv_nsec -= 1000000000l;
    }
    return t1;
}

static struct timespec
timesub (struct timespec t1, struct timespec t2)
{
    t1.tv_sec -= t2.tv_sec;
    if (t1.tv_nsec >= t2.tv_nsec) {
        t1.tv_nsec -= t2.tv_nsec;
    } else {
        t1.tv_sec--;
        t1.tv_nsec = t1.tv_nsec + 1000000000l - t2.tv_nsec;
    }
    return t1;
}

void transpose(int *dst, int *src, int dim) {
	int i, j;

	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			dst[j*dim + i] = src[i*dim + j];
}

void transpose2(int *dst, int *src, int dim) {
	int i, j;

	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			dst[j*dim + i] = src[i*dim + j];
}

void print_matrix(int *m, int dim) {
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {
			printf("%3d ", m[i*dim + j]);
		}
		printf("\n");
	}
	printf("\n");
}

#define N 1024
#define DURATION 10

int main(int argc, char *argv[]) {
	int dim = N;
	if (argc > 1) {
		dim = atoi(argv[1]);
	}

	size_t size = sizeof(int) * dim * dim;
	warnx("N=%d (%ldbytes)", dim, size);

	int *src = calloc(1, size);
	int *dst = calloc(1, size);
	int *ans = calloc(1, size);

	int v = 0;
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {
			src[i*dim+j] = v;
			ans[j*dim+i] = v;
			v++;
		}
	}

	// print_matrix(src, dim);

	// Check if transpose2 returns valid answer
	transpose2(dst, src, dim);
	if (memcmp((void*)dst, (void*)ans, size) != 0) {
		print_matrix(dst, dim);
		errx(1, "doesn't match");
	}
	warnx("transpose2 returned the expected result");

	memset((void*)dst, 0, sizeof(dst));

	struct timespec total1 = { 0, 0 }, total2 = { 0, 0 }, total3 = { 0, 0 }, total4 = { 0, 0 };
	struct timespec t1, t2, t3, t4, t5;
	unsigned long n = 0;

	signal(SIGALRM, sigalrm);
	warnx("start %u seconds test", DURATION);
	alarm(DURATION);

	while(!alrm){
		clock_gettime(CLOCK_MONOTONIC, &t1);
		transpose(dst, src, dim);
		clock_gettime(CLOCK_MONOTONIC, &t2);
		transpose2(dst, src, dim);
		clock_gettime(CLOCK_MONOTONIC, &t3);

	        total1 = timeadd(total1, timesub(t2, t1));
		total2 = timeadd(total2, timesub(t3, t2));

		n++;
	}

	errx (0, "n: %lu\n"
		"transpose : %ld.%09ld\n"
		"transpose2: %ld.%09ld\n",
		n, 
		total1.tv_sec, total1.tv_nsec, total2.tv_sec, total2.tv_nsec);
}
