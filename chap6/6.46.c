#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <err.h>
#include <stdlib.h>

static volatile int alrm;

static void
sigalrm (int sig)
{
    alrm = 1;
}

static struct timespec
timeadd (struct timespec t1, struct timespec t2)
{
    t1.tv_sec += t2.tv_sec;
    t1.tv_nsec += t2.tv_nsec;
    if (t1.tv_nsec >= 1000000000l) {
        t1.tv_sec++;
        t1.tv_nsec -= 1000000000l;
    }
    return t1;
}

static struct timespec
timesub (struct timespec t1, struct timespec t2)
{
    t1.tv_sec -= t2.tv_sec;
    if (t1.tv_nsec >= t2.tv_nsec) {
        t1.tv_nsec -= t2.tv_nsec;
    } else {
        t1.tv_sec--;
        t1.tv_nsec = t1.tv_nsec + 1000000000l - t2.tv_nsec;
    }
    return t1;
}

void col_convert(int *G, int dim) {
	int i, j;

	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			G[j*dim + i] = G[j*dim + i] || G[i*dim + j];
}

void col_convert2(int *G, int dim) {
	int i, j;

	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			G[j*dim + i] = G[j*dim + i] || G[i*dim + j];
}

void print_matrix(int *g, int *x, int dim) {
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {
			int p = i * dim + j;
			printf("%3d%c", x[p], (x[p] != g[p]) ? '*' : ' ');
		}
		printf("\n");
	}
	printf("\n");
}

#define N 5000
#define DURATION 10

int main(int argc, char *argv[]) {
	int dim = N;
	if (argc > 1) {
		dim = atoi(argv[1]);
	}

	size_t size = sizeof(int) * dim * dim;
	warnx("N=%d (%ldbytes)", dim, size);

	int *g = calloc(1, size);
	int *g1 = calloc(1, size);
	int *g2 = calloc(1, size);

	int v = 0;
	for (int i = 0; i < dim; i++)
		for (int j = 0; j < dim; j++)
			g[i*dim+j] = (random() % 10) == 0;

	memcpy(g1, g, size);
	memcpy(g2, g, size);

	// Check if tranpose2 returns valid answer
	col_convert(g1, dim);
	col_convert2(g2, dim);
	if (memcmp((void*)g1, (void*)g2, size) != 0) {
		print_matrix(g1, g2, dim);
		errx(1, "doesn't match");
	}
	warnx("col_convert2 returned the expected result");

	struct timespec total1 = { 0, 0 }, total2 = { 0, 0 };
	struct timespec t1, t2, t3;
	unsigned long n = 0;

	signal(SIGALRM, sigalrm);
	warnx("start %u seconds test", DURATION);
	alarm(DURATION);

	while(!alrm){
		memcpy(g1, g, size);
		memcpy(g2, g, size);

		clock_gettime(CLOCK_MONOTONIC, &t1);
		col_convert(g, dim);
		clock_gettime(CLOCK_MONOTONIC, &t2);
		col_convert2(g, dim);
		clock_gettime(CLOCK_MONOTONIC, &t3);

	        total1 = timeadd(total1, timesub(t2, t1));
		total2 = timeadd(total2, timesub(t3, t2));

		n++;
	}

	errx (0, "n: %lu\n"
		"col_convert : %ld.%09ld\n"
		"col_convert2: %ld.%09ld\n",
		n, 
		total1.tv_sec, total1.tv_nsec, total2.tv_sec, total2.tv_nsec);
}
